# Copyright (C) 2016 Intel Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import pytest
import shlex
import sys
from subprocess import Popen, PIPE

def python34_module(modulename):
    cmd = shlex.split("python3 -m %s" % modulename) 
    try:
        Popen(cmd, stdout=PIPE).communicate()[0]
    except:
        pytest.fail("Error: python module %s not present" % modulename)


class TestPython3Installed:

    @pytest.fixture
    def python3_version(self):
        cmd = shlex.split("python3 --version") 
        output = Popen(cmd, stdout=PIPE).communicate()[0]
        return output

    def test_python3_installed(self, python3_version):
        assert python3_version == 'Python 3.4.5\n'


class TestNewPython34Modules:

    def test_python34_asyncio(self):
         python34_module("asyncio")

    def test_python34_ensurepip(self):
        python34_module("ensurepip")

    def test_python34_enum(self):
        python34_module("enum")

    def test_python34_pathlib(self):
        python34_module("pathlib")

    def test_python34_selectors(self):
        python34_module("selectors")

    def test_python34_statistics(self):
        python34_module("statistics")

    def test_python34_tracemalloc(self):
        python34_module("tracemalloc")
