# Copyright (C) 2016 Intel Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import pytest
import shlex
from subprocess import Popen, PIPE

class TestPython2Macros:

    @pytest.fixture
    def python2_version_macro(self):
        cmd = shlex.split("rpm -E %python2_version")
        output = Popen(cmd, stdout=PIPE).communicate()[0]
        return output

    @pytest.fixture
    def python2_version_nodots_macro(self):
        cmd = shlex.split("rpm -E %python2_version_nodots")
        output = Popen(cmd, stdout=PIPE).communicate()[0]
        return output

    def test_python2_version_macro(self, python2_version_macro):
        assert python2_version_macro == '2.6\n'

    def test_python2_version_nodots_macro(self, python2_version_nodots_macro):
       assert python2_version_nodots_macro == '26\n'
