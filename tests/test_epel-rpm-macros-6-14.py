# Copyright (C) 2016 Intel Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import os
import pytest
import shlex
from subprocess import Popen, PIPE

class TestPython3Stubs:

    @pytest.fixture
    def py3_build_macro(self):
        cmd = shlex.split("rpm -E %py3_build")
        output = Popen(cmd, stdout=PIPE).communicate()[0]
        return output

    @pytest.fixture
    def py3_install_macro(self):
        cmd = shlex.split("rpm -E %py3_install")
        output = Popen(cmd, stdout=PIPE).communicate()[0]
        return output

    @pytest.fixture
    def nil_macro(self):
        cmd = shlex.split("rpm -E %{nil}")
        output = Popen(cmd, stdout=PIPE).communicate()[0]
        return output

    def test_py3_build_macro(self, py3_build_macro, nil_macro):
        assert py3_build_macro != nil_macro

    def test_py3_install_macro(self, py3_install_macro, nil_macro):
       assert py3_install_macro != nil_macro
