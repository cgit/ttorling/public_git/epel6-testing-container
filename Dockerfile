# Copyright (C) 2016 Intel Corporation
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License version 2 as
# published by the Free Software Foundation.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

FROM centos:6.8

RUN yum -y update; yum clean all
RUN yum -y install epel-release; yum clean all
RUN yum -y install rpm python python-pip epel-rpm-macros python-rpm-macros; yum clean all
RUN yum -y update --enablerepo=epel-testing; yum clean all
RUN yum -y install python34 --enablerepo=epel-testing; yum clean all
RUN pip install -U pytest
COPY tests/* tests/

CMD ["pytest", "tests/"]
