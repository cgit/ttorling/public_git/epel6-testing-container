# epel6-testing-container

Use a Centos 6.8 Docker container to run tests (pytest) to validate ```epel-testing``` features.

## TL;DR
```
./build_and_run.sh
```

which runs these commands for you
```
docker rmi epel6-testing
docker build -t epel6-testing .
docker run --rm -t epel6-testing
```

You __WILL__ want to delete old images when you want to get new updates from the ```epel-testing``` repo.


```
docker images
docker rmi <the offending old image hash>
```
